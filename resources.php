<?php


return [
    'resources' => [
        'bursar' => [
            \MiamiOH\RESTngBursar\Resources\BursarResourceProvider::class,
        ],
    ]
];

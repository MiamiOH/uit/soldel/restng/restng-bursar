<?php


namespace MiamiOH\RESTngBursar\Tests\Unit;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Schema\Blueprint;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngBursar\Models\SectionFees;
use MiamiOH\RESTngBursar\Models\DetailCharge;
use MiamiOH\RESTngBursar\Repositories\SSRFEESEloquent;
use MiamiOH\RESTngBursar\Services\Bursar;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use PHPUnit\Framework\TestCase;

class BursarTest extends TestCase {

    private static $table = 'SSRFEES';

    /** @var Request $request */
    private $request;

    /** @var Bursar $bursar */
    private $bursar;

    /* Called by PHPUnit before every test method */
    public function setUp(): void
    {
        parent::setUp();

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods([
                'getResourceParams',
                'getOptions',
                'getSubObjects',
                'getResourceParamKey',
                'getData'
            ])
            ->getMock();
        $this->bursar = new Bursar(new SSRFEESEloquent());
        $this->bursar->setRequest($this->request);

    }

    public static function setUpBeforeClass(): void
    {
        $dataSourceConfig = [
            'driver'   => 'sqlite',
            'database' => ':memory:'
        ];

        RESTngEloquentFactory::boot($dataSourceConfig);

        DB::schema()->create(self::$table, function (Blueprint $table) {
            $table->integer('ssrfees_surrogate_id')->default(1);
            $table->string('ssrfees_term_code');
            $table->string('ssrfees_crn', 5);
            $table->string('ssrfees_detl_code', 4);
            $table->string('ssrfees_amount', 20);
            $table->string('ssrfees_ftyp_code', 4);
            $table->string('ssrfees_user_id', 20);
            $table->string('ssrfees_seq_no', 100);
            $table->dateTime('ssrfees_activity_date');

        });

        DB::schema()->create('tbbdetc', function (Blueprint $table) {
            $table->string('tbbdetc_detail_code', 4)->unique();
            $table->string('tbbdetc_desc', 200);
        });
    }

    /*
     *   Empty Body Given
     * 	 Tests Case in which body is empty.
     *	 Expected Return: 400 Bad Request
     */
    public function testPostEmptyBody()
    {
        $this->request->method('getData')
            ->willReturn([]);

        $response = $this->bursar->createBursarRecord();
        $this->assertSame(400, $response->getStatus());

    }

    /** @dataProvider mockInvalidBursarData
     * @param $key
     * @param $payload
     * @param $message
     *
     * @throws \Exception
     */
    public function testValidationsForInvalidAndEmptyPostData($key, $payload, $message)
    {
        $this->request->method('getData')
            ->willReturn($payload);

        /*** Call the createBursarRecord and set the payload ***/
        $response = $this->bursar->createBursarRecord();
        $this->assertSame(400, $response->getStatus());
        $this->assertSame($message, $response->getPayload()['errors'][0]->getMessages()[$key][0]);
    }

    public function mockInvalidBursarData(): array
    {
        return [
            ['termCode', ['termCode' => '4312329'], 'Maximum length exceeded for term code.'],
            ['termCode', ['termCode' => null], 'The term code field is required.'],
            ['crn', ['crn' => null], 'The crn field is required.'],
            ['crn', ['crn' => '12254647586970'], 'Maximum length exceeded for crn.'],
            ['detailCode', ['detailCode' => null], 'The detail code field is required.'],
            ['detailCode', ['detailCode' => '12254647586970'], 'Maximum length exceeded for detail code.'],
            ['amount', ['amount' => null], 'The amount field is required.'],
            ['userId', ['userId' => null], 'The user id field is required.'],
        ];

    }

    public function testCanCreateBursarRecord(): void
    {
        $payload = [
            'termCode'   => '201930',
            'crn'        => '13890',
            'detailCode' => 'WON1',
            'amount'     => '2.5',
            'userId'     => 'UNIZIN',
            'fytpCode'   => 'FLAT',
        ];
        $this->request->method('getData')
            ->willReturn($payload);

        $this->assertSame(0, SectionFees::count());
        $response = $this->bursar->createBursarRecord();
        $this->assertSame(201, $response->getStatus());
        $this->assertSame(1, SectionFees::count());

        $insertedRecord = SectionFees::whereSsrfeesCrn('13890')->first();
        $this->assertSame('201930', $insertedRecord->ssrfees_term_code);
        $this->assertSame('13890', $insertedRecord->ssrfees_crn);
        $this->assertSame('WON1', $insertedRecord->ssrfees_detl_code);
        $this->assertSame('2.5', $insertedRecord->ssrfees_amount);
        $this->assertSame('UNIZIN', $insertedRecord->ssrfees_user_id);

    }

    /** @dataProvider  mockBursarGETRequestParams */
    public function testInvalidGETRequest($key, $params, $message): void
    {
        $this->request->method('getResourceParams')
            ->willReturn($params);

        $response = $this->bursar->getBursarRecord();
        $this->assertSame(400, $response->getStatus());
        $this->assertSame($message, $response->getPayload()['errors'][0]->getMessages()[$key][0]);
    }

    public function mockBursarGETRequestParams(): array
    {
        return [
            ['termCode', ['termCode' => '2019810910', 'crn' => '1212'], 'Maximum length exceeded for term code.'],
            ['termCode', ['termCode' => null, 'crn' => '1212'], 'The term code field is required.'],
            ['crn', ['termCode' => '201910', 'crn' => null], 'The crn field is required.'],
            ['crn', ['termCode' => '201910', 'crn' => '1234234'], 'Maximum length exceeded for crn.'],
        ];
    }

    public function testCanGetBursarData(): void
    {
        $expectedOutput = [
            [
                'termCode'              => '201910',
                'crn'                   => '1234',
                'detailCode'            => 'UART',
                'detailCodeDescription' => 'canvas e-textbook for ART',
                'amount'                => '25',
                'ftypCode'             => 'FLAT',
                'userId'                => 'UNIZIN',
            ]
        ];
        $this->request->method('getResourceParams')
            ->willReturn(['termCode' => '201910', 'crn' => '1234']);

        $this->generateFakeFeeData();
        $this->generateFakeDetailChargeData();

        $this->assertSame(200, $this->bursar->getBursarRecord()->getStatus());
        $this->assertSame($expectedOutput, $this->bursar->getBursarRecord()->getPayload());
    }

    protected function tearDown(): void
    {
        foreach ([self::$table, 'tbbdetc'] as $table)
        {
            DB::table($table)->delete();
        }
    }

    private function generateFakeFeeData(): void
    {
        SectionFees::create([
            'ssrfees_term_code' => '201910',
            'ssrfees_crn'       => '1234',
            'ssrfees_detl_code' => 'UART',
            'ssrfees_amount'    => '25',
            'ssrfees_user_id'   => 'UNIZIN',
            'ssrfees_ftyp_code' => 'FLAT',
            'ssrfees_seq_no'    => 1
        ]);
    }

    private function generateFakeDetailChargeData(): void
    {
        DetailCharge::create([
            'TBBDETC_DETAIL_CODE' => 'UART',
            'TBBDETC_DESC'        => 'canvas e-textbook for ART',
        ]);
    }

}
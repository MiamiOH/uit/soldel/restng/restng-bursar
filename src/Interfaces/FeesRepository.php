<?php


namespace MiamiOH\RESTngBursar\Interfaces;

use Illuminate\Support\Collection;
use MiamiOH\RESTngBursar\Models\SectionFees;

interface FeesRepository
{
    public function createFeeRecord(array $feeData): SectionFees;
    public function getBursarRecord(array $params): Collection;
}
<?php

namespace MiamiOH\RESTngBursar\Models;

use Illuminate\Database\Eloquent\Model;

class SectionFees extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ssrfees';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = true;

    protected $primaryKey = 'ssrfees_surrogate_id';

    private const ELOQUOENT_COLUMNS_MAP = [
            'termCode' => 'ssrfees_term_code',
            'crn' => 'ssrfees_crn',
            'detailCode' => 'ssrfees_detl_code',
            'amount' => 'ssrfees_amount',
            'userId' => 'ssrfees_user_id',
            'fytpCode' => 'ssrfees_ftyp_code',
        ];

    const CREATED_AT = 'ssrfees_activity_date';
    const UPDATED_AT = 'ssrfees_activity_date';

    protected $guarded = [];

    /** Mapping of SSRFEES column data to swagger data */
    public static function getEloquentColumns(array $data)
    {
        $feeEloquentData = [];
        foreach ($data as $key => $datum){
            $feeEloquentData[self::ELOQUOENT_COLUMNS_MAP[$key]] = $datum;
        }
        return $feeEloquentData;
    }

    public function charge()
    {
        return $this->belongsTo(DetailCharge::class, 'ssrfees_detl_code', 'tbbdetc_detail_code');
    }
}

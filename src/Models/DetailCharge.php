<?php

namespace MiamiOH\RESTngBursar\Models;

use Illuminate\Database\Eloquent\Model;

class DetailCharge extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbbdetc';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = 'tbbdetc_detail_code';

    protected $guarded = [];

    public function fees()
    {
        return $this->hasMany(SectionFees::class, 'ssrfees_detl_code', 'tbbdetc_detail_code');
    }
}

<?php

namespace MiamiOH\RESTngBursar\Models;

use Illuminate\Database\Eloquent\Model;

class StudentSection extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ssbsect';

    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    protected $primaryKey = null;
}

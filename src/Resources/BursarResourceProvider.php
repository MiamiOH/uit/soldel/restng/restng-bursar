<?php

namespace MiamiOH\RESTngBursar\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class BursarResourceProvider extends ResourceProvider
{
    private $tag = 'Bursar';
    private $name = 'Bursar';

    public function registerDefinitions(): void
    {
        $this->addTag([
            'name' => $this->tag,
            'description' => 'Bursar resource'
        ]);

        $this->addDefinition([
            'name' => 'Bursar.GET.Response',
            'type' => 'object',
            'properties' => [
                'termCode' => [
                    'type' => 'string',
                    'description' => 'Term code for which we are creating course section fee - SSRFEES_TERM_CODE/ Term code for the course section information - SSBSECT_TERM_CODE'
                ],
                'crn' => [
                    'type' => 'string',
                    'description' => 'Course Reference Number assigned to this course section - SSBSECT_CRN / CRN for which you are creating the course section fee. - SSRFEES_CRN'
                ],
                'detailCode' => [
                    'type' => 'string',
                    'description' => 'Detail code associated with every charge and payment that can be entered on an account - TBBDETC_DETAIL_CODE'
                ],
                'detailCodeDescription' => [
                    'type' => 'string',
                    'description' => 'Description of the detail code - TBBDETC_DESC'
                ],
                'sequenceNumber' => [
                    'type' => 'string',
                    'description' => 'This field identifies section number of a course. Section number can only be used once to identify course number combination in a term. - SSBSECT_SEQ_NUMB./ Sequence number for making a unique Index - SSRFEES_SEQ_NO'
                ],
                'fTypeCode' => [
                    'type' => 'string',
                    'description' => 'Fee Type Code: Used to identify the type of fee to fee assessment - SSRFEES_FTYP_CODE'
                ],
                'levelCode' => [
                    'type' => 'string',
                    'description' => 'Identifies the Level code that the fee is defined for - SSRFEES_LEVL_CODE'
                ],
                'amount' => [
                    'type' => 'string',
                    'description' => 'Amount of fee you are creating for the course section - SSRFEES_AMOUNT /  Default amount associated with the associated with the detail code - TBBDETC_AMOUNT'
                ],
                'userId' => [
                    'type' => 'string',
                    'description' => 'User who inserted or last updated the data - SSBSECT_USER_ID / User ID of a person who inserted or last updated this record. - TBBDETC_USER_ID / User ID: ID of a user who created or last updated this record - SSRFEES_USER_ID'
                ],
                'isbn' => [
                    'type' => 'string',
                    'description' => 'ISBN of the content request as input by the user. '
                ],
            ],
        ]); //GET Model definition

        $this->addDefinition([
            'name' => 'Bursar.POST.model',
            'type' => 'object',
            'properties' => [
                'termCode' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'default' => '202010',
                ],
                'crn' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'default' => '13880',
                ],
                'detailCode' => [
                    'type' => 'string',
                    'enum' => ['required|string'],
                    'default' => 'WON1',
                ],
                'amount' => [
                    'type' => 'string',
                    'default' => '2.5, 54',
                ],
                'userId' => [
                    'type' => 'string',
                    'default' => 'SATURN, null, UNIZIN',
                ],
            ],
        ]); //POST Model definition

        $this->addDefinition([
            'name' => 'Bursar.PUT.model',
            'type' => 'object',
            'properties' => [
                'termCode' => [
                    'type' => 'string',
                    'enum' => ['required|string']
                ],
                'crn' => [
                    'type' => 'string',
                ],
                'detailCode' => [
                    'type' => 'string',
                ],
                'detailCodeDescription' => [
                    'type' => 'string',
                ],
                'sequenceNumber' => [
                    'type' => 'string',
                ],
                'fTypeCode' => [
                    'type' => 'string',
                ],
                'levelCode' => [
                    'type' => 'string',
                ],
                'amount' => [
                    'type' => 'string',
                ],
                'userId' => [
                    'type' => 'string',
                ],
                'isbn' => [
                    'type' => 'string',
                ],
            ],
        ]); //PUT Model definition

    }

    public function registerServices(): void
    {

        $this->addService([
            'name' => 'FeesRepository',
            'class' => 'MiamiOH\RESTngBursar\Repositories\SSRFEESEloquent',
            'description' => 'SSRFEES Eloquent Repository'
        ]);

        $this->addService([
            'name' => 'Bursar',
            'class' => 'MiamiOH\RESTngBursar\Services\Bursar',
            'description' => 'Provide CRUD interface to Bursar model',
            'params' => [
                'bursarRepository' => [
                    'type' => 'service',
                    'name' => 'FeesRepository']
            ],
            'set' => [
                'database' => [
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ],
                'dataSource' => [
                    'type' => 'service',
                    'name' => 'APIDataSource'
                ],
            ]
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'create',
            'description' => 'Create new bursar record',
            'name' => 'Create Bursar Data Model',
            'service' => $this->name,
            'method' => 'createBursarRecord',
            'tags' => ['Bursar'],
            'pattern' => '/bursar/fee/v1',
            'body' => [
                'required' => true,
                'description' => 'Bursar Data',
                'schema' => [
                    '$ref' => '#/definitions/' . 'Bursar.POST.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Created successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],

            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['create']
                ],
            ]
        ]); // POST/Create new bursar resource

        $this->addResource([
            'action' => 'read',
            'description' => 'Retrieve bursar record',
            'name' => $this->name . '.GET.response',
            'pattern' => '/bursar/fee/v1/:termCode/:crn',
            'service' => 'Bursar',
            'method' => 'getBursarRecord',
            'returnType' => 'collection',
            'tags' => ['Bursar'],
            'params' => [
                'termCode' => [
                    'description' => 'Term code for the course section information'
                ],
                'crn' => [
                    'description' => 'CRN for which you are creating the course section fee'
                ],
            ],
            'middleware' => [
                'authenticate' => [
                    [
                        'type' => 'anonymous'
                    ],
                    [
                        'type' => 'token'
                    ],
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A Bursar collection',
                    'returns' => [
                        'type' => 'model',
                        '$ref' => '#/definitions/Bursar.GET.Response'
                    ]
                ],

                App::API_NOTFOUND => [
                    'description' => 'The requested termCode could not be found'
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized Access',
                ]
            ]

        ]); // GET/Retrieve the bursar resource

        $this->addResource([
            'action' => 'update',
            'description' => 'Update bursar record',
            'name' => $this->name . '.PUT.model',
            'service' => $this->name,
            'method' => 'updateBursarRecord',
            'tags' => ['Bursar'],
            'pattern' => '/bursar/fee/v1',
            'body' => [
                'required' => true,
                'description' => 'Bursar data',
                'schema' => [
                    '$ref' => '#/definitions/' . 'Bursar.PUT.model'
                ]
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'Update successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => $this->tag,
                    'key' => ['update', 'edit', 'all']
                ],
            ]
        ]); // PUT/Update the bursar resource
    }

    public function registerOrmConnections(): void
    {
        // TODO: Implement registerOrmConnections() method.
    }
}
<?php

namespace MiamiOH\RESTngBursar\Services;

use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTngBursar\Interfaces\FeesRepository;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class Bursar extends Service
{
    private $dbh;
    private $dbDataSourceName = 'MUWS_SEC_PROD';

    /************************************************/
    /**
     * @var FeesRepository bursarRepository
     */
    private $bursarRepository;

    /**
     * Bursar constructor.
     * @param FeesRepository $bursarRepository
     */
    public function __construct(FeesRepository $bursarRepository)
    {
        $this->bursarRepository = $bursarRepository;
    }

    /**********Setter Dependency Injection**********
     * @param DataSource $dataSourceManager
     */
    /***********************************************/

    public function setDataSource(DataSource $dataSourceManager)
    {
        $dataSourceConfig = $dataSourceManager->getDataSource($this->dbDataSourceName);

        RESTngEloquentFactory::boot($dataSourceConfig);
    }

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);

    }

    private $postRequestRules = [
        'termCode' => 'required|max:6',
        'crn' => 'required|max:5',
        'detailCode' => 'required|max:4',
        'detailCodeDescription' => 'max:255',
        'sequenceNumber' => 'max:5',
        'fTypeCode' => 'max:4',
        'levelCode' => 'max:2',
        'amount' => 'required',
        'userId' => 'required',
        'isbn' => ''
    ];

    private $getRequestRules = [
        'termCode' => 'required|max:6',
        'crn' => 'required|max:5',
    ];

    private $messages = [
        'required' => 'The :attribute field is required.',
        'max' => 'Maximum length exceeded for :attribute.',
    ];

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function createBursarRecord()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = $request->getData();

        # Make sure data is not empty
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;
            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        $validator = RESTngValidatorFactory::make($request->getData(), $this->postRequestRules, $this->messages);
        if ($validator->fails())
        {
            return $this->getValidationErrors($validator, $response);
        }
        $this->bursarRepository->createFeeRecord($validator->getData());
        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload(['1 record created.']);

        return $response;
    }

    public function getBursarRecord()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $validator = RESTngValidatorFactory::make($request->getResourceParams(), $this->getRequestRules, $this->messages);
        if ($validator->fails())
        {
            return $this->getValidationErrors($validator, $response);
        }

        $records = $this->bursarRepository->getBursarRecord($validator->getData())->toArray();
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($records);

        return $response;

    }

    private function getValidationErrors($validator, $response)
    {
        $errors = $validator->errors();
        $payLoad['errors'][] = $errors->getMessageBag();
        $response->setPayLoad($payLoad);
        $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
        return $response;
    }
}
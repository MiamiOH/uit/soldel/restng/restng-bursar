<?php


namespace MiamiOH\RESTngBursar\Repositories;


use Illuminate\Support\Collection;
use MiamiOH\RESTngBursar\Interfaces\FeesRepository;
use MiamiOH\RESTngBursar\Models\SectionFees;

class SSRFEESEloquent implements FeesRepository
{
    public function createFeeRecord(array $feeData): SectionFees
    {
        $feeModelData = SectionFees::getEloquentColumns($feeData);
        $sequenceNumber = SectionFees::select('ssrfees_seq_no')
            ->whereSsrfeesTermCode($feeModelData['ssrfees_term_code'])
            ->whereSsrfeesCrn($feeModelData['ssrfees_crn'])
            ->whereSsrfeesDetlCode($feeModelData['ssrfees_detl_code'])
            ->OrderByDesc('ssrfees_seq_no')->first();
        $sequenceNumber = $sequenceNumber ? $sequenceNumber->ssrfees_seq_no + 1 : 1;
        $feeModelData['ssrfees_seq_no'] = $sequenceNumber;
        return SectionFees::create($feeModelData);
    }

    public function getBursarRecord(array $params): Collection
    {
        $feeRecords = collect();
        $feeModelData = SectionFees::getEloquentColumns($params);
        SectionFees::with('charge')->where($feeModelData)->get()->each(function ($sectionFeeRecord) use (&$feeRecords){
            $feeRecord['termCode'] = $sectionFeeRecord->ssrfees_term_code ?? null;
            $feeRecord['crn'] = $sectionFeeRecord->ssrfees_crn ?? null;
            $feeRecord['detailCode'] = $sectionFeeRecord->ssrfees_detl_code ?? null;
            $feeRecord['detailCodeDescription'] = $sectionFeeRecord->charge->tbbdetc_desc ?? null;
            $feeRecord['amount'] = $sectionFeeRecord->ssrfees_amount ?? null;
            $feeRecord['ftypCode'] = $sectionFeeRecord->ssrfees_ftyp_code ?? null;
            $feeRecord['userId'] = $sectionFeeRecord->ssrfees_user_id ?? null;
            $feeRecords->push($feeRecord);
        });
        return $feeRecords;
    }
}